<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomErrorPageController extends Controller
{
    //
    public function PageNotFound()
    {
        return view('views.errors.404');
    }
}
