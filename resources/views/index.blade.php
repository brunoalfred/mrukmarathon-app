@extends('layouts.base')

@section('content')

<main class="site-main post-8 envato_tk_templates type-envato_tk_templates status-publish has-post-thumbnail hentry"
    role="main">
    <div class="page-content">
        <div data-elementor-type="wp-post" data-elementor-id="8" class="elementor elementor-8"
            data-elementor-settings="[]">
            <div class="elementor-inner">
                <div class="elementor-section-wrap">
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-7e465f33 elementor-section-height-min-height elementor-section-items-bottom elementor-section-boxed elementor-section-height-default"
                        data-id="7e465f33" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;background_motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;background_motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;background_motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:5,&quot;sizes&quot;:[]},&quot;background_motion_fx_devices&quot;:[&quot;desktop&quot;],&quot;background_motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:100}}}">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3210ed8f"
                                    data-id="3210ed8f" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-7f7fb7d7 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="7f7fb7d7" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-4c4cc542"
                                                            data-id="4c4cc542" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-6fd49628 elementor-widget elementor-widget-heading"
                                                                        data-id="6fd49628" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h1 class="elementor-heading-title elementor-size-default">
                                                                                Mr Uk Marathon
                                                                            </h1>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-23dd9e24 elementor-widget elementor-widget-text-editor"
                                                                        data-id="23dd9e24" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-text-editor elementor-clearfix">
                                                                                <p>Run towards Investment</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-2ca9a0d7 elementor-tablet-align-center elementor-widget elementor-widget-button"
                                                                        data-id="2ca9a0d7" data-element_type="widget"
                                                                        data-widget_type="button.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-button-wrapper">
                                                                                <a class="elementor-button-link elementor-button elementor-size-sm"
                                                                                    role="button">
                                                                                    <span
                                                                                        class="elementor-button-content-wrapper">
                                                                                        <span
                                                                                            class="elementor-button-text">Register Here</span>
                                                                                    </span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-2063ec91"
                                                            data-id="2063ec91" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-ea539d6 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="ea539d6" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-b27a36d"
                                                            data-id="b27a36d" data-element_type="column">
                                                            <div class="elementor-column-wrap">
                                                                <div class="elementor-widget-wrap">
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0add82a"
                                                            data-id="0add82a" data-element_type="column"
                                                            data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-7ba0756c elementor-countdown--label-block elementor-widget elementor-widget-countdown"
                                                                        data-id="7ba0756c" data-element_type="widget"
                                                                        data-widget_type="countdown.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-countdown-wrapper"
                                                                                data-date="1616682420">
                                                                                <div class="elementor-countdown-item">
                                                                                    <span
                                                                                        class="elementor-countdown-digits elementor-countdown-days"></span>
                                                                                    <span
                                                                                        class="elementor-countdown-label">Days</span>
                                                                                </div>
                                                                                <div class="elementor-countdown-item">
                                                                                    <span
                                                                                        class="elementor-countdown-digits elementor-countdown-hours"></span>
                                                                                    <span
                                                                                        class="elementor-countdown-label">Hours</span>
                                                                                </div>
                                                                                <div class="elementor-countdown-item">
                                                                                    <span
                                                                                        class="elementor-countdown-digits elementor-countdown-minutes"></span>
                                                                                    <span
                                                                                        class="elementor-countdown-label">Minutes</span>
                                                                                </div>
                                                                                <div class="elementor-countdown-item">
                                                                                    <span
                                                                                        class="elementor-countdown-digits elementor-countdown-seconds"></span>
                                                                                    <span
                                                                                        class="elementor-countdown-label">Seconds</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-41c09bd9 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="41c09bd9" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-3e6687f7"
                                    data-id="3e6687f7" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-e1033c1 elementor-widget__width-initial elementor-absolute elementor-widget elementor-widget-image"
                                                data-id="e1033c1" data-element_type="widget"
                                                data-settings="{&quot;_position&quot;:&quot;absolute&quot;}"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="300" height="496"
                                                            src="../../wp-content/uploads/sites/12/2020/03/dots-min.png"
                                                            class="attachment-large size-large" alt="" loading="lazy"
                                                            srcset="https://kits.pixel-show.com/eventico/wp-content/uploads/sites/12/2020/03/dots-min.png 300w, https://kits.pixel-show.com/eventico/wp-content/uploads/sites/12/2020/03/dots-min-181x300.png 181w"
                                                            sizes="(max-width: 300px) 100vw, 300px" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-3b1040e elementor-widget elementor-widget-image"
                                                data-id="3b1040e" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img src="{{asset('assets/wp-content/uploads/sites/12/elementor/thumbs/MarathonDemo.jpg')}}"
                                                            title="crowd-at-a-concert-PEM4VFD-min"
                                                            alt="crowd-at-a-concert-PEM4VFD-min" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-43ec6e28"
                                    data-id="43ec6e28" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-1523c4fb elementor-widget elementor-widget-heading"
                                                data-id="1523c4fb" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h3 class="elementor-heading-title elementor-size-default">RACE</h3>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-71bc8dc9 elementor-widget elementor-widget-text-editor"
                                                data-id="71bc8dc9" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <p>The race itself is only half the fun. Featuring interactive
                                                            exhibits, live entertainment, product trials and all things
                                                            marathon, the Health & Fitness Expo is one of the highlights
                                                            of race weekend.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-3e6d287c elementor-tablet-align-center elementor-widget elementor-widget-button"
                                                data-id="3e6d287c" data-element_type="widget"
                                                data-widget_type="button.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a href="#"
                                                            class="elementor-button-link elementor-button elementor-size-sm"
                                                            role="button">
                                                            <span class="elementor-button-content-wrapper">
                                                                <span class="elementor-button-text">
                                                                    Register Here</span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-e793ba0 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="e793ba0" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-3c83523"
                                    data-id="3c83523" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-1a5c733 elementor-widget elementor-widget-heading"
                                                data-id="1a5c733" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h3 class="elementor-heading-title elementor-size-default">MR UK
                                                        MARATHON
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-438262c elementor-widget elementor-widget-text-editor"
                                                data-id="438262c" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <p>The MR UK marathon health & fitness expo hosts over 10
                                                            exhibitors featuring brand-new designs in running gear and
                                                            shoes,
                                                            as well as the latest developments in sports, fitness, and
                                                            nutrition. The Health & Fitness Expo is free and open to the
                                                            public.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-3a807cc elementor-tablet-align-center elementor-widget elementor-widget-button"
                                                data-id="3a807cc" data-element_type="widget"
                                                data-widget_type="button.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a href="#"
                                                            class="elementor-button-link elementor-button elementor-size-sm"
                                                            role="button">
                                                            <span class="elementor-button-content-wrapper">
                                                                <span class="elementor-button-text">Register Here</span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-01b1f12"
                                    data-id="01b1f12" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-9bf8e4f elementor-widget elementor-widget-image"
                                                data-id="9bf8e4f" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img src="{{asset('assets/wp-content/uploads/sites/12/elementor/thumbs/Mruk marathon.jpg')}}"
                                                            title="camera-at-a-media-conference-PFUKE2U"
                                                            alt="camera-at-a-media-conference-PFUKE2U" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-c888eca elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="c888eca" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-3c9d9910"
                                    data-id="3c9d9910" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-6222d7b5 elementor-widget elementor-widget-heading"
                                                data-id="6222d7b5" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h3 class="elementor-heading-title elementor-size-default">About
                                                        This
                                                        Event
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-552c7e5e elementor-widget elementor-widget-text-editor"
                                                data-id="552c7e5e" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <p>
                                                            "The mr Uk marathon aims at uplifting the spirit of sports in the
                                                            investment sector." 
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-562ade0f elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="562ade0f" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-7f7bd48d"
                                    data-id="7f7bd48d" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-7e2929f2 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="7e2929f2" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-667dc98e"
                                                            data-id="667dc98e" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-3146abe elementor-widget__width-initial elementor-absolute elementor-widget elementor-widget-heading"
                                                                        data-id="3146abe" data-element_type="widget"
                                                                        data-settings="{&quot;_position&quot;:&quot;absolute&quot;}"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                1</div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-621f08fb elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-image-box"
                                                                        data-id="621f08fb" data-element_type="widget"
                                                                        data-widget_type="image-box.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image-box-wrapper">
                                                                                <figure class="elementor-image-box-img">
                                                                                    <img width="128" height="128"
                                                                                        src="{{asset('assets/wp-content/uploads/sites/12/2020/03/icon-blue-3-min.png')}}"
                                                                                        class="attachment-full size-full"
                                                                                        alt="" loading="lazy" />
                                                                                </figure>
                                                                                <div
                                                                                    class="elementor-image-box-content">
                                                                                    <h5
                                                                                        class="elementor-image-box-title">
                                                                                       Good Health</h5>
                                                                                    <p
                                                                                        class="elementor-image-box-description">
                                                                                        Everything boils downs to strong health of mind and Body.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-11b25df4"
                                    data-id="11b25df4" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-37170f8 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="37170f8" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-14d7a68f"
                                                            data-id="14d7a68f" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-3aab150a elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-image-box"
                                                                        data-id="3aab150a" data-element_type="widget"
                                                                        data-widget_type="image-box.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image-box-wrapper">
                                                                                <figure class="elementor-image-box-img">
                                                                                    <img width="128" height="128"
                                                                                        src="{{asset('assets/wp-content/uploads/sites/12/2020/03/icon-blue-1-min.png')}}"
                                                                                        class="attachment-full size-full"
                                                                                        alt="" loading="lazy" />
                                                                                </figure>
                                                                                <div
                                                                                    class="elementor-image-box-content">
                                                                                    <h5
                                                                                        class="elementor-image-box-title">
                                                                                        Commitment</h5>
                                                                                    <p
                                                                                        class="elementor-image-box-description">
                                                                                        Working smart and commitedly towards personal goals</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-af40d63 elementor-widget__width-initial elementor-absolute elementor-widget elementor-widget-heading"
                                                                        data-id="af40d63" data-element_type="widget"
                                                                        data-settings="{&quot;_position&quot;:&quot;absolute&quot;}"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                2</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-6bd2ac60"
                                    data-id="6bd2ac60" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-6161527d elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="6161527d" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-526c42fe"
                                                            data-id="526c42fe" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-733338fa elementor-position-left elementor-vertical-align-top elementor-widget elementor-widget-image-box"
                                                                        data-id="733338fa" data-element_type="widget"
                                                                        data-widget_type="image-box.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image-box-wrapper">
                                                                                <figure class="elementor-image-box-img">
                                                                                    <img width="128" height="128"
                                                                                        src="{{asset('assets/wp-content/uploads/sites/12/2020/03/icon-blue-6-min.png')}}"
                                                                                        class="attachment-full size-full"
                                                                                        alt="" loading="lazy" />
                                                                                </figure>
                                                                                <div
                                                                                    class="elementor-image-box-content">
                                                                                    <h5
                                                                                        class="elementor-image-box-title">
                                                                                        Success</h5>
                                                                                    <p
                                                                                        class="elementor-image-box-description">
                                                                                        Achieving the fruits of your investment.</p>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-c8d3a7e elementor-widget__width-initial elementor-absolute elementor-widget elementor-widget-heading"
                                                                        data-id="c8d3a7e" data-element_type="widget"
                                                                        data-settings="{&quot;_position&quot;:&quot;absolute&quot;}"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                3</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-5d7dec2a elementor-section-height-min-height elementor-section-items-bottom elementor-section-boxed elementor-section-height-default"
                        data-id="5d7dec2a" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;,&quot;background_motion_fx_motion_fx_scrolling&quot;:&quot;yes&quot;,&quot;background_motion_fx_translateY_effect&quot;:&quot;yes&quot;,&quot;background_motion_fx_translateY_speed&quot;:{&quot;unit&quot;:&quot;px&quot;,&quot;size&quot;:5,&quot;sizes&quot;:[]},&quot;background_motion_fx_devices&quot;:[&quot;desktop&quot;],&quot;background_motion_fx_translateY_affectedRange&quot;:{&quot;unit&quot;:&quot;%&quot;,&quot;size&quot;:&quot;&quot;,&quot;sizes&quot;:{&quot;start&quot;:0,&quot;end&quot;:100}}}">
                        <div class="elementor-background-overlay"></div>
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4accbf59"
                                    data-id="4accbf59" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-a239f elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="a239f" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-282b92b7"
                                                            data-id="282b92b7" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-da7d152 elementor-widget elementor-widget-heading"
                                                                        data-id="da7d152" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                What are you waiting for?</h2>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-6d1e2d48 elementor-widget elementor-widget-text-editor"
                                                                        data-id="6d1e2d48" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <p> Grow  stronger and  healthy with friends.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-c2fd4ac elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="c2fd4ac" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-8ece410"
                                                            data-id="8ece410" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-b8cc82d elementor-widget elementor-widget-counter"
                                                                        data-id="b8cc82d" data-element_type="widget"
                                                                        data-widget_type="counter.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-counter">
                                                                                <div
                                                                                    class="elementor-counter-number-wrapper">
                                                                                    <span
                                                                                        class="elementor-counter-number-prefix"></span>
                                                                                    <span
                                                                                        class="elementor-counter-number"
                                                                                        data-duration="2000"
                                                                                        data-to-value="195"
                                                                                        data-from-value="0"
                                                                                        data-delimiter=",">0</span>
                                                                                    <span
                                                                                        class="elementor-counter-number-suffix"></span>
                                                                                </div>
                                                                                <div class="elementor-counter-title">
                                                                                   from all over the<br>  world</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-b833041"
                                                            data-id="b833041" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-f6d3368 elementor-widget elementor-widget-counter"
                                                                        data-id="f6d3368" data-element_type="widget"
                                                                        data-widget_type="counter.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-counter">
                                                                                <div
                                                                                    class="elementor-counter-number-wrapper">
                                                                                    <span
                                                                                        class="elementor-counter-number-prefix"></span>
                                                                                    <span
                                                                                        class="elementor-counter-number"
                                                                                        data-duration="2000"
                                                                                        data-to-value="3599"
                                                                                        data-from-value="0"
                                                                                        data-delimiter=",">0</span>
                                                                                    <span
                                                                                        class="elementor-counter-number-suffix">+</span>
                                                                                </div>
                                                                                <div class="elementor-counter-title">
                                                                                  Total Runners<br>to participate</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-33 elementor-inner-column elementor-element elementor-element-4945ed7"
                                                            data-id="4945ed7" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-6317afc elementor-widget elementor-widget-counter"
                                                                        data-id="6317afc" data-element_type="widget"
                                                                        data-widget_type="counter.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-counter">
                                                                                <div
                                                                                    class="elementor-counter-number-wrapper">
                                                                                    <span
                                                                                        class="elementor-counter-number-prefix"></span>
                                                                                    <span
                                                                                        class="elementor-counter-number"
                                                                                        data-duration="2000"
                                                                                        data-to-value="1"
                                                                                        data-from-value="0"
                                                                                        data-delimiter=",">0</span>
                                                                                    <span
                                                                                        class="elementor-counter-number-suffix"></span>
                                                                                </div>
                                                                                <div class="elementor-counter-title">
                                                                                    unforgettable <br>day</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-8041cd4 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="8041cd4" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-100 elementor-inner-column elementor-element elementor-element-9d81f9e"
                                                            data-id="9d81f9e" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-13f5ce0 elementor-align-center elementor-widget elementor-widget-button"
                                                                        data-id="13f5ce0" data-element_type="widget"
                                                                        data-widget_type="button.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-button-wrapper">
                                                                                <a href="#"
                                                                                    class="elementor-button-link elementor-button elementor-size-sm"
                                                                                    role="button">
                                                                                    <span
                                                                                        class="elementor-button-content-wrapper">
                                                                                        <span
                                                                                            class="elementor-button-text">Register Here</span>
                                                                                    </span>
                                                                                </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-583e3f0e elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="583e3f0e" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-193e181b"
                                    data-id="193e181b" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-753e6b49 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="753e6b49" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-488782c5"
                                                            data-id="488782c5" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-99cfcac elementor-widget elementor-widget-heading"
                                                                        data-id="99cfcac" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                Sponsors<br>and
                                                                                <br>Patners
                                                                            </h2>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-c609f78"
                                                            data-id="c609f78" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-377a978 elementor-widget elementor-widget-heading"
                                                                        data-id="377a978" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h2
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                ”</h2>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-ee096fc elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="ee096fc" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-eb98200"
                                    data-id="eb98200" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-a67266d elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="a67266d" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-84830c2"
                                                            data-id="84830c2" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-b4ddbb2 elementor-widget elementor-widget-image"
                                                                        data-id="b4ddbb2" data-element_type="widget"
                                                                        data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <a href="#">
                                                                                    <img src="{{asset('assets/wp-content/uploads/sites/12/elementor/thumbs/face-7-min-onbvp48bfwsiiorq45kvfa74usf9bloxkof31ufs8o.jpg')}}"
                                                                                        title="face-7-min"
                                                                                        alt="face-7-min" /> </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-be842b3"
                                                            data-id="be842b3" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-8aeba57 elementor-widget elementor-widget-heading"
                                                                        data-id="8aeba57" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h5
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                <a href="#">June Lopez</a></h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-8ed3d07 elementor-widget elementor-widget-text-editor"
                                                                        data-id="8ed3d07" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <p>Lorem ipsum dolor sit amet,
                                                                                    consectetur adipiscing elit, sed do
                                                                                    eiusmod tempor incididunt ut labore
                                                                                    et dolore magna.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-30d15a4"
                                    data-id="30d15a4" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-7b9bc73 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="7b9bc73" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-eac5454"
                                                            data-id="eac5454" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-92546c0 elementor-widget elementor-widget-image"
                                                                        data-id="92546c0" data-element_type="widget"
                                                                        data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <a href="#">
                                                                                    <img src="{{asset('assets/wp-content/uploads/sites/12/elementor/thumbs/face-6-min-onbvp48bfwsiiorq45kvfa74usf9bloxkof31ufs8o.jpg')}}"
                                                                                        title="face-6-min"
                                                                                        alt="face-6-min" /> </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-b804c98"
                                                            data-id="b804c98" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-f88733a elementor-widget elementor-widget-heading"
                                                                        data-id="f88733a" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h5
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                <a href="#">Kenneth Desimone</a></h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-f80df57 elementor-widget elementor-widget-text-editor"
                                                                        data-id="f80df57" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <p>Lorem ipsum dolor sit amet,
                                                                                    consectetur adipiscing elit, sed do
                                                                                    eiusmod tempor incididunt ut labore
                                                                                    et dolore magna.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-ae48f8f elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="ae48f8f" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-84d76df"
                                    data-id="84d76df" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-06767fd elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="06767fd" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0465570"
                                                            data-id="0465570" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-e2371bc elementor-widget elementor-widget-image"
                                                                        data-id="e2371bc" data-element_type="widget"
                                                                        data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <a href="#">
                                                                                    <img src="{{asset('assets/wp-content/uploads/sites/12/elementor/thumbs/face-9-min-onbvp3ah92r872t39n68usfo9ejw3wl78jrlkkh6ew.jpg')}}"
                                                                                        title="face-9-min"
                                                                                        alt="face-9-min" /> </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-0861ddd"
                                                            data-id="0861ddd" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-0e5b55c elementor-widget elementor-widget-heading"
                                                                        data-id="0e5b55c" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h5
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                <a href="#">David Applegate</a></h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-f4a7a7e elementor-widget elementor-widget-text-editor"
                                                                        data-id="f4a7a7e" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <p>Lorem ipsum dolor sit amet,
                                                                                    consectetur adipiscing elit, sed do
                                                                                    eiusmod tempor incididunt ut labore
                                                                                    et dolore magna.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-f301ea1"
                                    data-id="f301ea1" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <section
                                                class="elementor-section elementor-inner-section elementor-element elementor-element-47e6bc8 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                                                data-id="47e6bc8" data-element_type="section">
                                                <div class="elementor-container elementor-column-gap-default">
                                                    <div class="elementor-row">
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-d6fbf0c"
                                                            data-id="d6fbf0c" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-44428ba elementor-widget elementor-widget-image"
                                                                        data-id="44428ba" data-element_type="widget"
                                                                        data-widget_type="image.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div class="elementor-image">
                                                                                <a href="#">
                                                                                    <img src="{{asset('assets/wp-content/uploads/sites/12/elementor/thumbs/face-5-min-onbvp3ah92r872t39n68usfo9ejw3wl78jrlkkh6ew.jpg')}}"
                                                                                        title="face-5-min"
                                                                                        alt="face-5-min" /> </a>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="elementor-column elementor-col-50 elementor-inner-column elementor-element elementor-element-e87c98f"
                                                            data-id="e87c98f" data-element_type="column">
                                                            <div
                                                                class="elementor-column-wrap elementor-element-populated">
                                                                <div class="elementor-widget-wrap">
                                                                    <div class="elementor-element elementor-element-1e87ed6 elementor-widget elementor-widget-heading"
                                                                        data-id="1e87ed6" data-element_type="widget"
                                                                        data-widget_type="heading.default">
                                                                        <div class="elementor-widget-container">
                                                                            <h5
                                                                                class="elementor-heading-title elementor-size-default">
                                                                                <a href="#">Laurie Jackson</a></h5>
                                                                        </div>
                                                                    </div>
                                                                    <div class="elementor-element elementor-element-6aed4b3 elementor-widget elementor-widget-text-editor"
                                                                        data-id="6aed4b3" data-element_type="widget"
                                                                        data-widget_type="text-editor.default">
                                                                        <div class="elementor-widget-container">
                                                                            <div
                                                                                class="elementor-text-editor elementor-clearfix">
                                                                                <p>Lorem ipsum dolor sit amet,
                                                                                    consectetur adipiscing elit, sed do
                                                                                    eiusmod tempor incididunt ut labore
                                                                                    et dolore magna.</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-55531f4 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="55531f4" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-4ba88eda"
                                    data-id="4ba88eda" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-1438571a elementor-widget elementor-widget-heading"
                                                data-id="1438571a" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h2 class="elementor-heading-title elementor-size-default">
                                                        Everything is well organized
                                                    </h2>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-26af6e68 elementor-widget elementor-widget-text-editor"
                                                data-id="26af6e68" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <p>
                                                            The crew has ensured that all neccessary services are well 
                                                            prepared to make this event comfortable for all participates.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-50 elementor-top-column elementor-element elementor-element-32cf966e"
                                    data-id="32cf966e" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-773283c elementor-widget elementor-widget-progress"
                                                data-id="773283c" data-element_type="widget"
                                                data-widget_type="progress.default">
                                                <div class="elementor-widget-container">
                                                    <span class="elementor-title">Free Parkings</span>

                                                    <div class="elementor-progress-wrapper" role="progressbar"
                                                        aria-valuemin="0" aria-valuemax="100" aria-valuenow="80"
                                                        aria-valuetext="">
                                                        <div class="elementor-progress-bar" data-max="89">
                                                            <span class="elementor-progress-text"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-adbdbf5 elementor-widget elementor-widget-progress"
                                                data-id="adbdbf5" data-element_type="widget"
                                                data-widget_type="progress.default">
                                                <div class="elementor-widget-container">
                                                    <span class="elementor-title">Food and Drinks</span>

                                                    <div class="elementor-progress-wrapper" role="progressbar"
                                                        aria-valuemin="0" aria-valuemax="100" aria-valuenow="68"
                                                        aria-valuetext="">
                                                        <div class="elementor-progress-bar" data-max="88">
                                                            <span class="elementor-progress-text"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-24c830c elementor-widget elementor-widget-progress"
                                                data-id="24c830c" data-element_type="widget"
                                                data-widget_type="progress.default">
                                                <div class="elementor-widget-container">
                                                    <span class="elementor-title">Comfortable washrooms</span>

                                                    <div class="elementor-progress-wrapper" role="progressbar"
                                                        aria-valuemin="0" aria-valuemax="100" aria-valuenow="70"
                                                        aria-valuetext="">
                                                        <div class="elementor-progress-bar" data-max="90">
                                                            <span class="elementor-progress-text"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-6ba5f2e elementor-widget elementor-widget-progress"
                                                data-id="6ba5f2e" data-element_type="widget"
                                                data-widget_type="progress.default">
                                                <div class="elementor-widget-container">
                                                    <span class="elementor-title">Medical Care</span>

                                                    <div class="elementor-progress-wrapper" role="progressbar"
                                                        aria-valuemin="0" aria-valuemax="100" aria-valuenow="95"
                                                        aria-valuetext="">
                                                        <div class="elementor-progress-bar" data-max="95">
                                                            <span class="elementor-progress-text"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-adbdbf5 elementor-widget elementor-widget-progress" data-id="adbdbf5"
                                                data-element_type="widget" data-widget_type="progress.default">
                                                <div class="elementor-widget-container">
                                                    <span class="elementor-title">Simple registration process</span>
                                            
                                                    <div class="elementor-progress-wrapper" role="progressbar" aria-valuemin="0" aria-valuemax="100"
                                                        aria-valuenow="68" aria-valuetext="">
                                                        <div class="elementor-progress-bar" data-max="98">
                                                            <span class="elementor-progress-text"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-7b0ec452 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="7b0ec452" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-28508be1"
                                    data-id="28508be1" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-6b65ad98 elementor-widget elementor-widget-heading"
                                                data-id="6b65ad98" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h3 class="elementor-heading-title elementor-size-default">Amazing
                                                        Sponsors<br> & Partners</h3>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-3950373 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="3950373" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-291d872c"
                                    data-id="291d872c" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-7bd5532 elementor-widget elementor-widget-image"
                                                data-id="7bd5532" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="249" height="248"
                                                            src="{{asset('assets/wp-content/uploads/sites/12/2020/03/the_logos_60_white_19.png')}}"
                                                            class="attachment-large size-large" alt="" loading="lazy"
                                                            srcset="https://kits.pixel-show.com/eventico/wp-content/uploads/sites/12/2020/03/the_logos_60_white_19.png 249w, https://kits.pixel-show.com/eventico/wp-content/uploads/sites/12/2020/03/the_logos_60_white_19-150x150.png 150w"
                                                            sizes="(max-width: 249px) 100vw, 249px" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-7914daa"
                                    data-id="7914daa" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-e21cef3 elementor-widget elementor-widget-image"
                                                data-id="e21cef3" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="197" height="245"
                                                            src="{{asset('assets/wp-content/uploads/sites/12/2020/03/the_logos_60_white_16.png')}}"
                                                            class="attachment-large size-large" alt="" loading="lazy" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-0736af5"
                                    data-id="0736af5" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-0bb7162 elementor-widget elementor-widget-image"
                                                data-id="0bb7162" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="249" height="245"
                                                            src="{{asset('assets/wp-content/uploads/sites/12/2020/03/the_logos_60_white_15.png')}}"
                                                            class="attachment-large size-large" alt="" loading="lazy" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-7930de3"
                                    data-id="7930de3" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-d1d915f elementor-widget elementor-widget-image"
                                                data-id="d1d915f" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="213" height="244"
                                                            src="{{asset('assets/wp-content/uploads/sites/12/2020/03/the_logos_60_white_09.png')}}"
                                                            class="attachment-large size-large" alt="" loading="lazy" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-4f55465 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="4f55465" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-3f6fb37"
                                    data-id="3f6fb37" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-e0123d2 elementor-widget elementor-widget-image"
                                                data-id="e0123d2" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="302" height="244"
                                                            src="{{asset('assets/wp-content/uploads/sites/12/2020/03/the_logos_60_white_05.png')}}"
                                                            class="attachment-large size-large" alt="" loading="lazy"
                                                            srcset="https://kits.pixel-show.com/eventico/wp-content/uploads/sites/12/2020/03/the_logos_60_white_05.png 302w, https://kits.pixel-show.com/eventico/wp-content/uploads/sites/12/2020/03/the_logos_60_white_05-300x242.png 300w"
                                                            sizes="(max-width: 302px) 100vw, 302px" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-efd0e66"
                                    data-id="efd0e66" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-8052d11 elementor-widget elementor-widget-image"
                                                data-id="8052d11" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="197" height="248"
                                                            src="{{asset('assets/wp-content/uploads/sites/12/2020/03/the_logos_60_white_20.png')}}"
                                                            class="attachment-large size-large" alt="" loading="lazy" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-c5abf75"
                                    data-id="c5abf75" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-875ea18 elementor-widget elementor-widget-image"
                                                data-id="875ea18" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="249" height="244"
                                                            src="{{asset('assets/wp-content/uploads/sites/12/2020/03/the_logos_60_white_03.png')}}"
                                                            class="attachment-large size-large" alt="" loading="lazy" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-25 elementor-top-column elementor-element elementor-element-b5f453e"
                                    data-id="b5f453e" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-5e88a4d elementor-widget elementor-widget-image"
                                                data-id="5e88a4d" data-element_type="widget"
                                                data-widget_type="image.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-image">
                                                        <img width="197" height="244"
                                                            src="{{asset('assets/wp-content/uploads/sites/12/2020/03/the_logos_60_white_07.png')}}"
                                                            class="attachment-large size-large" alt="" loading="lazy" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-6c77069f elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="6c77069f" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-52731f6d"
                                    data-id="52731f6d" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-5cc5271f elementor-widget elementor-widget-heading"
                                                data-id="5cc5271f" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h3 class="elementor-heading-title elementor-size-default">Have
                                                        Question?<br> Contact Us!</h3>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-7808fd32 elementor-widget elementor-widget-text-editor"
                                                data-id="7808fd32" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <p>In case of any inquiry or anything you would like to share with us, we are happy to 
                                                            hear from you.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-3e6fe913 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="3e6fe913" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-364096df"
                                    data-id="364096df" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-668e5ace elementor-widget elementor-widget-heading"
                                                data-id="668e5ace" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h6 class="elementor-heading-title elementor-size-default">Where
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-21890a96 elementor-widget elementor-widget-text-editor"
                                                data-id="21890a96" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">Kariakoo<br>Dar Es Salaam</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-3ec8174"
                                    data-id="3ec8174" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-24c26320 elementor-widget elementor-widget-heading"
                                                data-id="24c26320" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h6 class="elementor-heading-title elementor-size-default">When</h6>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-1f0c0a55 elementor-widget elementor-widget-text-editor"
                                                data-id="1f0c0a55" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">Monday to
                                                        Saturday<br> in working hours</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-1479134"
                                    data-id="1479134" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-7f5008e9 elementor-widget elementor-widget-heading"
                                                data-id="7f5008e9" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h6 class="elementor-heading-title elementor-size-default">Email
                                                    </h6>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-18469563 elementor-widget elementor-widget-text-editor"
                                                data-id="18469563" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <p>info@mrukmarathon.run</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-9d4e7b6 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="9d4e7b6" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-81ca0d8"
                                    data-id="81ca0d8" data-element_type="column"
                                    data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-1fd0c00 elementor-button-align-stretch elementor-widget elementor-widget-form"
                                                data-id="1fd0c00" data-element_type="widget"
                                                data-settings="{&quot;button_width&quot;:&quot;100&quot;,&quot;step_next_label&quot;:&quot;Next&quot;,&quot;step_previous_label&quot;:&quot;Previous&quot;,&quot;step_type&quot;:&quot;number_text&quot;,&quot;step_icon_shape&quot;:&quot;circle&quot;}"
                                                data-widget_type="form.default">
                                                <div class="elementor-widget-container">
                                                    <form class="elementor-form" method="post" name="New Form">
                                                        <input type="hidden" name="post_id" value="8" />
                                                        <input type="hidden" name="form_id" value="1fd0c00" />

                                                        <input type="hidden" name="queried_id" value="8" />

                                                        <div class="elementor-form-fields-wrapper elementor-labels-">
                                                            <div
                                                                class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-name elementor-col-100 elementor-field-required">
                                                                <label for="form-field-name"
                                                                    class="elementor-field-label elementor-screen-only">Name*</label><input
                                                                    size="1" type="text" name="form_fields[name]"
                                                                    id="form-field-name"
                                                                    class="elementor-field elementor-size-sm  elementor-field-textual"
                                                                    placeholder="Name*" required="required"
                                                                    aria-required="true">
                                                            </div>
                                                            <div
                                                                class="elementor-field-type-email elementor-field-group elementor-column elementor-field-group-email elementor-col-100 elementor-field-required">
                                                                <label for="form-field-email"
                                                                    class="elementor-field-label elementor-screen-only">Email*</label><input
                                                                    size="1" type="email" name="form_fields[email]"
                                                                    id="form-field-email"
                                                                    class="elementor-field elementor-size-sm  elementor-field-textual"
                                                                    placeholder="Email*" required="required"
                                                                    aria-required="true">
                                                            </div>
                                                            <div
                                                                class="elementor-field-type-text elementor-field-group elementor-column elementor-field-group-field_1 elementor-col-100">
                                                                <label for="form-field-field_1"
                                                                    class="elementor-field-label elementor-screen-only">Subject</label><input
                                                                    size="1" type="text" name="form_fields[field_1]"
                                                                    id="form-field-field_1"
                                                                    class="elementor-field elementor-size-sm  elementor-field-textual"
                                                                    placeholder="Subject">
                                                            </div>
                                                            <div
                                                                class="elementor-field-type-textarea elementor-field-group elementor-column elementor-field-group-message elementor-col-100">
                                                                <label for="form-field-message"
                                                                    class="elementor-field-label elementor-screen-only">Message</label><textarea
                                                                    class="elementor-field-textual elementor-field  elementor-size-sm"
                                                                    name="form_fields[message]" id="form-field-message"
                                                                    rows="4" placeholder="Message"></textarea>
                                                            </div>
                                                            <div
                                                                class="elementor-field-group elementor-column elementor-field-type-submit elementor-col-100 e-form__buttons">
                                                                <button type="submit"
                                                                    class="elementor-button elementor-size-sm">
                                                                    <span>
                                                                        <span class=" elementor-button-icon">
                                                                        </span>
                                                                        <span class="elementor-button-text">Send
                                                                            Message</span>
                                                                    </span>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-524ce403 elementor-section-stretched elementor-section-full_width elementor-section-height-default elementor-section-height-default"
                        data-id="524ce403" data-element_type="section"
                        data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-1c917fdc"
                                    data-id="1c917fdc" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-17e9e5b4 elementor-widget elementor-widget-google_maps"
                                                data-id="17e9e5b4" data-element_type="widget"
                                                data-widget_type="google_maps.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-custom-embed"><iframe frameborder="0"
                                                            scrolling="no" marginheight="0" marginwidth="0"
                                                            src="https://maps.google.com/maps?q=mr%20uk%20&t=&z=13&ie=UTF8&iwloc=&output=embed"
                                                            title="121 King Street Melbourne, 3000, Australia"
                                                            aria-label="121 King Street Melbourne, 3000, Australia"></iframe>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section
                        class="elementor-section elementor-top-section elementor-element elementor-element-16c42385 elementor-section-boxed elementor-section-height-default elementor-section-height-default"
                        data-id="16c42385" data-element_type="section"
                        data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
                        <div class="elementor-container elementor-column-gap-default">
                            <div class="elementor-row">
                                <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-192334b6"
                                    data-id="192334b6" data-element_type="column">
                                    <div class="elementor-column-wrap elementor-element-populated">
                                        <div class="elementor-widget-wrap">
                                            <div class="elementor-element elementor-element-75939e60 elementor-widget elementor-widget-heading"
                                                data-id="75939e60" data-element_type="widget"
                                                data-widget_type="heading.default">
                                                <div class="elementor-widget-container">
                                                    <h3 class="elementor-heading-title elementor-size-default">Don't plan to miss</h3>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-883ad8c elementor-widget elementor-widget-text-editor"
                                                data-id="883ad8c" data-element_type="widget"
                                                data-widget_type="text-editor.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-text-editor elementor-clearfix">
                                                        <p>It's going to be a fun day for families together with alot of games for Kids.</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="elementor-element elementor-element-3a358b1f elementor-align-center elementor-widget elementor-widget-button"
                                                data-id="3a358b1f" data-element_type="widget"
                                                data-widget_type="button.default">
                                                <div class="elementor-widget-container">
                                                    <div class="elementor-button-wrapper">
                                                        <a href="#"
                                                            class="elementor-button-link elementor-button elementor-size-sm"
                                                            role="button">
                                                            <span class="elementor-button-content-wrapper">
                                                                <span class="elementor-button-text">Register Here</span>
                                                            </span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="post-tags">
        </div>
    </div>

    <section id="comments" class="comments-area">




    </section>

</main>

@endsection