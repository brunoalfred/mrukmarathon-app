<div data-elementor-type="footer" data-elementor-id="460" class="elementor elementor-460 elementor-location-footer" data-elementor-settings="[]">
    <div class="elementor-section-wrap">
        <section class="elementor-section elementor-top-section elementor-element elementor-element-7136bba5 elementor-section-boxed elementor-section-height-default elementor-section-height-default" data-id="7136bba5" data-element_type="section">
            <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-row">
                    <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-4f039542" data-id="4f039542" data-element_type="column">
                        <div class="elementor-column-wrap elementor-element-populated">
                            <div class="elementor-widget-wrap">
                                <div class="elementor-element elementor-element-6e571a22 elementor-widget elementor-widget-image" data-id="6e571a22" data-element_type="widget" data-widget_type="image.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-image">
                                            <a href="#">
                                                <img width="126" height="91" src="https://res.cloudinary.com/ddkhh5kin/image/upload/c_scale,w_55/v1613410149/mrukmarathon-assets/images/logo/logo_crsqp8.png" class="attachment-full size-full" alt="" loading="lazy" /> </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="elementor-element elementor-element-20b15b97 elementor-widget elementor-widget-heading" data-id="20b15b97" data-element_type="widget" data-widget_type="heading.default">
                                    <div class="elementor-widget-container">
                                        <h6 class="elementor-heading-title elementor-size-default">© mrukmarathon. 2021 .<br> All rights reserved.

                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>