<div data-elementor-type="header" data-elementor-id="461" class="elementor elementor-461 elementor-location-header"
    data-elementor-settings="[]">
    <div class="elementor-section-wrap">
        <section
            class="elementor-section elementor-top-section elementor-element elementor-element-a3669fe elementor-section-boxed elementor-section-height-default elementor-section-height-default"
            data-id="a3669fe" data-element_type="section"
            data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
            <div class="elementor-container elementor-column-gap-default">
                <div class="elementor-row">
                    <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-9b27a22"
                        data-id="9b27a22" data-element_type="column">
                        <div class="elementor-column-wrap elementor-element-populated">
                            <div class="elementor-widget-wrap">
                                <div class="elementor-element elementor-element-5e28b86 elementor-widget elementor-widget-theme-site-logo elementor-widget-image"
                                    data-id="5e28b86" data-element_type="widget"
                                    data-widget_type="theme-site-logo.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-image">
                                            <a href="https://mrukmarathon.run">
                                                <img width="126" height="91"
                                                    src="https://res.cloudinary.com/ddkhh5kin/image/upload/c_scale,w_55/v1613410149/mrukmarathon-assets/images/logo/logo_crsqp8.png"
                                                    class="attachment-full size-full" alt="" loading="lazy" /> </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-99a6163"
                        data-id="99a6163" data-element_type="column">
                        <div class="elementor-column-wrap elementor-element-populated">
                            <div class="elementor-widget-wrap">
                                <div class="elementor-element elementor-element-1c046b3 elementor-nav-menu__align-right elementor-nav-menu--indicator-chevron elementor-absolute elementor-nav-menu--dropdown-tablet elementor-nav-menu__text-align-aside elementor-nav-menu--toggle elementor-nav-menu--burger elementor-widget elementor-widget-nav-menu"
                                    data-id="1c046b3" data-element_type="widget"
                                    data-settings="{&quot;_position&quot;:&quot;absolute&quot;,&quot;layout&quot;:&quot;horizontal&quot;,&quot;toggle&quot;:&quot;burger&quot;}"
                                    data-widget_type="nav-menu.default">
                                    <div class="elementor-widget-container">
                                        <nav role="navigation"
                                            class="elementor-nav-menu--main elementor-nav-menu__container elementor-nav-menu--layout-horizontal e--pointer-none">
                                            <ul id="menu-1-1c046b3" class="elementor-nav-menu">
                                                <li
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-435">
                                                    <a href="../venue/index.html" class="elementor-item">Home</a></li>

                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-427">
                                                    <a href="#" class="elementor-item elementor-item-anchor">About</a>
                                                    <ul class="sub-menu elementor-nav-menu--dropdown">
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-429">
                                                            <a href="../about-event/index.html"
                                                                class="elementor-sub-item">Event</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-431">
                                                            <a href="../schedule/index.html"
                                                                class="elementor-sub-item">Sponsors</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-433">
                                                            <a href="../speakers/index.html"
                                                                class="elementor-sub-item">Route Maps</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-432">
                                                            <a href="../venue/index.html"
                                                                class="elementor-sub-item">Results</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-430">
                                                            <a href="../registration/index.html"
                                                                class="elementor-sub-item">Entry</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-428">
                                                            <a href="../contact-us/index.html"
                                                                class="elementor-sub-item">Our Crew</a></li>
                                                    </ul>
                                                </li>

                                                <li
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-422">
                                                    <a href="../../index.html" class="elementor-item">News & Media</a>
                                                    <ul class="sub-menu elementor-nav-menu--dropdown">
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-423">
                                                            <a href="../home-1/index.html"
                                                                class="elementor-sub-item">Media</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-424">
                                                            <a href="../home-2/index.html"
                                                                class="elementor-sub-item">Video</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-425">
                                                            <a href="index.html" aria-current="page"
                                                                class="elementor-sub-item elementor-item-active">Latest
                                                                News </a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-426">
                                                            <a href="../home-4/index.html"
                                                                class="elementor-sub-item">Results</a></li>
                                                    </ul>
                                                </li>

                                                <li
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-434">
                                                    <a href="../contact-us/index.html" class="elementor-item">Contact
                                                        Us</a></li>
                                            </ul>
                                        </nav>
                                        <div class="elementor-menu-toggle" role="button" tabindex="0"
                                            aria-label="Menu Toggle" aria-expanded="false">
                                            <i class="eicon-menu-bar" aria-hidden="true"></i>
                                            <span class="elementor-screen-only">Menu</span>
                                        </div>
                                        <nav class="elementor-nav-menu--dropdown elementor-nav-menu__container"
                                            role="navigation" aria-hidden="true">
                                            <ul id="menu-2-1c046b3" class="elementor-nav-menu">
                                                <li
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-422">
                                                    <a href="#" class="elementor-item">Home</a>
                                                    <ul class="sub-menu elementor-nav-menu--dropdown">
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-423">
                                                            <a href="../home-1/index.html"
                                                                class="elementor-sub-item">Home v1</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-424">
                                                            <a href="../home-2/index.html"
                                                                class="elementor-sub-item">Home v2</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item menu-item-425">
                                                            <a href="index.html" aria-current="page"
                                                                class="elementor-sub-item elementor-item-active">Home
                                                                v3</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-426">
                                                            <a href="../home-4/index.html"
                                                                class="elementor-sub-item">Home v4</a></li>
                                                    </ul>
                                                </li>
                                                <li
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-427">
                                                    <a href="#" class="elementor-item elementor-item-anchor">Pages</a>
                                                    <ul class="sub-menu elementor-nav-menu--dropdown">
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-429">
                                                            <a href="../about-event/index.html"
                                                                class="elementor-sub-item">About Event</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-431">
                                                            <a href="../schedule/index.html"
                                                                class="elementor-sub-item">Schedule</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-433">
                                                            <a href="../speakers/index.html"
                                                                class="elementor-sub-item">Speakers</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-432">
                                                            <a href="../venue/index.html"
                                                                class="elementor-sub-item">Venue</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-430">
                                                            <a href="../registration/index.html"
                                                                class="elementor-sub-item">Registration</a></li>
                                                        <li
                                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-428">
                                                            <a href="../contact-us/index.html"
                                                                class="elementor-sub-item">Contact Us</a></li>
                                                    </ul>
                                                </li>
                                                <li
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-435">
                                                    <a href="../venue/index.html" class="elementor-item">Venue</a></li>
                                                <li
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-434">
                                                    <a href="../contact-us/index.html" class="elementor-item">Contact
                                                        Us</a></li>
                                            </ul>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="elementor-column elementor-col-33 elementor-top-column elementor-element elementor-element-eff1ccd"
                        data-id="eff1ccd" data-element_type="column">
                        <div class="elementor-column-wrap elementor-element-populated">
                            <div class="elementor-widget-wrap">
                                <div class="elementor-element elementor-element-38bf86c elementor-align-right elementor-hidden-phone elementor-widget elementor-widget-button"
                                    data-id="38bf86c" data-element_type="widget" data-widget_type="button.default">
                                    <div class="elementor-widget-container">
                                        <div class="elementor-button-wrapper">
                                            <a href="#" class="elementor-button-link elementor-button elementor-size-sm"
                                                role="button">
                                                <span class="elementor-button-content-wrapper">
                                                    <span class="elementor-button-text">Register Here</span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>